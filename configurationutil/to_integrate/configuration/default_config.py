import os

import logging_helper
from base_config_db import BaseConfigurationDB, add_config_db, CONFIG_TEMP_PATH

logging = logging_helper.setup_logging()


DEFAULT_CONFIG_BASE_PATH = os.path.dirname(__file__)
DEFAULT_CONFIG_FILENAME = u'default_config.db'

DEFAULT_CONFIG_TEXT_PATH = u'{path}{sep}default_config'.format(path=DEFAULT_CONFIG_BASE_PATH,
                                                               sep=os.sep)

DEFAULT_CONFIG_DATABASES = []


def update_database_filepath(new_base):
    global DEFAULT_CONFIG_DATABASE_FILEPATH
    DEFAULT_CONFIG_DATABASE_FILEPATH = u'{path}{sep}{filename}'.format(path=new_base,
                                                                       sep=os.sep,
                                                                       filename=DEFAULT_CONFIG_FILENAME)

    logging.debug(u'Updating Default Config DB file path: {p}'.format(p=DEFAULT_CONFIG_DATABASE_FILEPATH))


# Ensure base path is set!
update_database_filepath(DEFAULT_CONFIG_BASE_PATH)

# Ensure This modules database is registered.
add_config_db(db_list=DEFAULT_CONFIG_DATABASES,
              db_path=DEFAULT_CONFIG_DATABASE_FILEPATH)


class DefaultConfigurationDB(BaseConfigurationDB):

    def __init__(self, *args, **kwargs):

        if len(DEFAULT_CONFIG_DATABASES) == 0:
            raise Exception(u'No default databases configured!')

        if len(DEFAULT_CONFIG_DATABASES) > 1:

            update_database_filepath(new_base=CONFIG_TEMP_PATH)

            if not os.path.exists(DEFAULT_CONFIG_DATABASE_FILEPATH):

                logging.info(u'Multiple Default databases, creating temporary default database!')

                bdb = BaseConfigurationDB(DEFAULT_CONFIG_DATABASE_FILEPATH)
                bdb.update_template_data(databases=DEFAULT_CONFIG_DATABASES)

        super(DefaultConfigurationDB, self).__init__(db_path=DEFAULT_CONFIG_DATABASE_FILEPATH,
                                                     record_changes=True,
                                                     *args,
                                                     **kwargs)


if __name__ == u'__main__':

    db = DefaultConfigurationDB()
    # db.load_config(path=DEFAULT_CONFIG_TEXT_PATH)                    # Re-initialise from text files
    db.load_config_updates(path=DEFAULT_CONFIG_TEXT_PATH)            # Update from text files
    db.cleanup()
