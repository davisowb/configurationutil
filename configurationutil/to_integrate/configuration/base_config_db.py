import datetime
from future.builtins import str
import os
import shutil
import sqlite3
from ast import literal_eval
from collections import OrderedDict
from tempfile import mkdtemp

import logging_helper

from dbhelperutil.database_handler import DatabaseHandler

logging = logging_helper.setup_logging()

CONFIG_TEMP_PATH = mkdtemp(prefix=u'ITT')


class BaseConfigurationDB(DatabaseHandler):

    """
    Please note it is the responsibility of the developer to run
    the cleanup function to tidy up temporary files.
    """

    def __init__(self,
                 *args,
                 **kwargs):
        super(BaseConfigurationDB, self).__init__(*args, **kwargs)

    def update_template_data(self,
                             databases,
                             use_change_logs=False):

        """
        Initialise configuration database from existing template databases.

        @param databases: dictionary of databases: paths
        @param use_change_logs: Boolean

        """

        self.open()

        # Get this DB's creation date
        db_created = self.get_formatted_row(table=u'general_config',
                                            select_columns=[u'value'],
                                            where=[{u'column': u'parameter',
                                                    u'value': u'db_creation_date',
                                                    u'operator': u'='}]).get(u'value')

        if db_created:
            db_created = datetime.datetime.strptime(db_created, u'%Y-%m-%d %H:%M:%S')

        else:
            db_created = datetime.datetime.strptime(u'1970-01-01', u'%Y-%m-%d')

        logging.debug(u'DB Created date: {d}'.format(d=db_created))

        # Get currently applied templates
        self.select(table=u'general_config',
                    select_columns=[u'value'],
                    where=[{u'column': u'parameter',
                            u'value': u'Template',
                            u'operator': u'='}])

        applied_templates = {}
        for _, template in enumerate(self.fetchall_formatted()):
            t = literal_eval(template.get(u'value'))
            applied_templates[t[0]] = t[1]

        logging.debug(u'Applied Templates: {t}'.format(t=applied_templates))

        templates_required = []
        reinit = False

        for database in databases:

            db_func = database[u'db']
            path = database[u'path']

            logging.debug(u'Initialising config template from: {path}'.format(path=path))

            logging_helper.ensure_path_exists(path)

            # Open Database
            template_db = db_func(db_path=path)
            template_db.open()

            # Check template DB creation date
            template_db_created = template_db.get_formatted_row(table=u'general_config',
                                                                select_columns=[u'value'],
                                                                where=[{u'column': u'parameter',
                                                                        u'value': u'db_creation_date',
                                                                        u'operator': u'='}]).get(u'value')

            if template_db_created:
                template_db_created = datetime.datetime.strptime(template_db_created, u'%Y-%m-%d %H:%M:%S')

            else:
                template_db_created = datetime.datetime.strptime(u'1970-01-01', u'%Y-%m-%d')

            logging.debug(u'Template Created date: {d}'.format(d=template_db_created))

            # Check whether source DB has been re-initialised
            if template_db_created > db_created:
                logging.warning(u'Template DB ({tp}) has been re-initialised!  Forcing re-initialisation of {p}...'
                                .format(tp=path,
                                        p=self.db_path))

                reinit = True
                break

            else:
                try:
                    # Get version of current template db
                    current_template = (path.split(os.sep)[-1], template_db.get_db_version())
                    logging.debug(u'Current Template: {t}'.format(t=current_template))

                    # Check if current template already loaded
                    applied_version = 0
                    if current_template[0] in applied_templates:
                        logging.debug(u'Found Template: {t}'.format(t=current_template))

                        applied_version = applied_templates.get(current_template[0])
                        logging.debug(u'Applied Version: {t}'.format(t=applied_version))

                        if current_template[1] > applied_version:
                            templates_required.append({u'template': current_template,
                                                       u'db': template_db,
                                                       u'applied_version': applied_version})

                        else:
                            logging.debug(u'Not applying template ({t}) to database as it has already been applied!'
                                          .format(t=current_template))

                    else:
                        templates_required.append({u'template': current_template,
                                                   u'db': template_db,
                                                   u'applied_version': applied_version})

                except Exception as err:
                    raise Exception(u'Database upgrade failed!\n{e}'.format(e=err))

            # Close Databases
            template_db.close()

        if reinit:
            self.close()
            self.reinit_db()

            logging.warning(u'Re-initialisation of {p} completed!'.format(p=self.db_path))

        elif templates_required:
            logging.info(u'===========================DATABASE UPGRADE REQUIRED===========================')
            logging.info(u'Upgrading database ({p})...'.format(p=self.db_path))

            for template in templates_required:
                self.apply_template(template_data=template,
                                    applied_templates=applied_templates,
                                    use_change_logs=use_change_logs)

            logging.info(u'===============================================================================')

        self.close()

    def apply_template(self,
                       template_data,
                       applied_templates,
                       use_change_logs):

        template = template_data.get(u'template')
        template_db = template_data.get(u'db')
        applied_version = template_data.get(u'applied_version')

        logging.info(u'Applying Template ({t}) to database.'.format(t=template))

        # Set load function based on use_change_logs parameter
        load_func = self.__load_template_data_from_change_logs if use_change_logs else self.__load_template_data

        # Run the load function for the template data
        template_db.open()
        load_func(db=template_db, version=applied_version)
        template_db.close()

        # Register loaded template
        previous_template = applied_templates.get(template[0])

        self.update_record(table=u'general_config',
                           values={u'parameter': u'Template',
                                   u'value': str(template)},
                           where=[{u'column': u'parameter',
                                   u'value': u'Template',
                                   u'operator': u'='},
                                  {u'column': u'value',
                                   u'value': str((template[0], previous_template)) if previous_template else None,
                                   u'operator': u'=',
                                   u'condition': u'AND'}])

        self.save()

        logging.info(u'Template ({t}) applied to database.'.format(t=template))

    def __load_template_data(self,
                             db,
                             version=0):

        """
        Add contents of passed database to this config database

        @param db_func: The database to be loaded.
        """

        current_tables = self.get_tables()
        tables = db.get_tables()

        for table in tables:

            if table not in self.default_tables:

                table_data = db.get_table_data_dictionary(table_name=table)

                name = table_data[u'table_name']
                headings = table_data[u'headings']
                rows = table_data[u'rows']

                columns = OrderedDict()
                for i in range(0, len(headings)):
                    columns[headings[i]] = u'TEXT'

                # If the table does not exist create the table!
                if table not in current_tables:
                    self.create_table(table=name, columns=columns)

                for i in range(0, len(rows)):
                    self.insert(table, headings, rows[i])

                self.save()

    def __load_template_data_from_change_logs(self,
                                              db,
                                              version=0):

        """
        Add contents of passed database to this config database from their change logs

        @param db: The database to be loaded.
        """

        db.select(table=u'db_change_log',
                  order_by=[u'id'],
                  where=[{u'column': u'db_version',
                          u'value': version,
                          u'operator': u'>'}])
        rows = db.fetchall()

        logging.debug(u'LOG ROWS: {r}'.format(r=rows))

        for row in rows:

            self.insert(u'db_change_log',
                        columns=[u'db_root_name',
                                 u'db_version',
                                 u'sql_command',
                                 u'sql_params'],
                        values=[row[1],
                                row[2],
                                row[3],
                                row[4]])

            logging.debug(u'LOG ROW Statement: {r}'.format(r=row[3]))
            logging.debug(u'LOG ROW Params str  : {r}'.format(r=row[4]))

            params = literal_eval(row[4])
            logging.debug(u'LOG ROW Params tuple: {r}'.format(r=params))

            # Run statement
            try:
                self.execute(statement=row[3], params=params)

            except sqlite3.OperationalError as err:
                logging.warning(u'Cannot run template statement; statement: {s}; params: {p}; Msg: {e}'
                                .format(s=row[3],
                                        p=params,
                                        e=err))

            self.save()

    def reinit_db(self):
        """
        Override this to reinitialise database!
        """

        self.reinitialise_db(backup=True)

    @staticmethod
    def cleanup():
        logging.info(u'Cleaning up Temp files.')

        if os.path.exists(CONFIG_TEMP_PATH):
            logging.debug(CONFIG_TEMP_PATH)
            shutil.rmtree(CONFIG_TEMP_PATH)


def add_config_db(db_list, db_path):

    db_info = {u'db': BaseConfigurationDB,
               u'path': db_path}

    # Ensure db is not already registered to avoid duplicate data
    if db_info not in db_list:
        db_list.append(db_info)
        logging.debug(db_list)


def dump_config(db_path, text_path):
    db = BaseConfigurationDB(db_path)
    db.dump_config(path=text_path)
    db.cleanup()
