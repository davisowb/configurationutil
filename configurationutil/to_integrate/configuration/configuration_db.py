import os
from tempfile import gettempdir

from appdirs import user_data_dir

import logging_helper
from base_config_db import BaseConfigurationDB
from default_config import DEFAULT_CONFIG_DATABASES

logging = logging_helper.setup_logging()


CONFIG_APPNAME = u'itt_general'

CONFIG_DATABASE_PATH = user_data_dir(appname=CONFIG_APPNAME)

CONFIG_DATABASE_FILE = (u'{path}{sep}{filename}'
                        .format(path=CONFIG_DATABASE_PATH,
                                sep=os.sep,
                                filename=u'configuration.db'))

DB_FIRST_RUN = (True
                if not os.path.exists(CONFIG_DATABASE_FILE)
                else False)

GENERAL_CONFIG = u'general_config'

# TODO: Write a decorator to check if the db is already
#       open and if not open and close it
#       This would be for select/delete/update/insert statements!


class ConfigurationDB(BaseConfigurationDB):

    def __init__(self,
                 *args,
                 **kwargs):

        self.db_path = (kwargs.get(u'db_path')
                        if kwargs.get(u'db_path')
                        else CONFIG_DATABASE_FILE)
        kwargs[u'db_path'] = self.db_path
        logging.debug(u'configuration DB Path: {p}'
                      .format(p=self.db_path))

        self.__run_setup_general = False

        super(ConfigurationDB, self).__init__(*args, **kwargs)

        self.init_config_db()

    def reinit_db(self):
        self.reinitialise_db(backup=True)
        self.init_config_db()

    def init_config_db(self):
        self.__initialise_temp_path()
        self.update_template_data(databases=DEFAULT_CONFIG_DATABASES,
                                  use_change_logs=True)

    def __initialise_temp_path(self):

        temp_path = u'{p}{sep}{d}'.format(p = gettempdir(),
                                          sep = os.sep,
                                          d = u'ITT_Temp_Data')

        self.initialise_parameter(parameter=u'Temp Path',
                                  value = temp_path)

        logging_helper.ensure_path_exists(temp_path)

    def initialise_parameter(self,
                             parameter,
                             value):

        # Initialise the parameter if it doesn't already exist
        try:
            self[parameter]
        except KeyError:

            # Doesn't exist: Add it to the table
            self.open()

            logging.info(u'{parameter} not configured, configuring...'
                         .format(parameter=parameter))

            self.insert(table=u'general_config',
                        columns=[u'parameter',
                                 u'value'],
                        values=[parameter,
                                value])
            self.save()

            self.close()

            logging.info(u'{parameter} initialised to: {value}'
                         .format(parameter=parameter,
                                 value=value))

    def get_temp_path(self,
                      folder=None):

        try:
            folder_path = self[u'Temp Path']
        except KeyError:
            folder_path = None

        if folder:
            folder_path = os.sep.join((folder_path, folder))

        logging_helper.ensure_path_exists(folder_path)

        return folder_path

    def get(self,
            parameter,
            default = None):
        try:
            return self[parameter]
        except KeyError:
            return default

    def __getitem__(self,
                    parameter):

        self.open()

        row = self.get_formatted_row(table=GENERAL_CONFIG,
                                     where=[{u'column': u'parameter',
                                             u'value': parameter,
                                             u'operator': u'='}])
        self.close()

        return row[u'value']

    def set(self,
            parameter,
            value,
            force_init = False):

        try:
            self[parameter]
        except KeyError:
            if force_init:
                self.initialise_parameter(parameter=parameter,
                                          value=value)
            return
        else:
            self.open()

            self.update(table=GENERAL_CONFIG,
                        values = {u'value': value},
                        where = {u'column':   u'parameter',
                                 u'value':    parameter,
                                 u'operator': u'='})

            self.save()
            self.close()

if __name__ == u'__main__':
    db = ConfigurationDB()
