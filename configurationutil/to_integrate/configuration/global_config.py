import os

import logging_helper
from base_config_db import (BaseConfigurationDB,
                            add_config_db,
                            CONFIG_TEMP_PATH)

logging = logging_helper.setup_logging()


GLOBAL_CONFIG_BASE_PATH = os.path.dirname(__file__)
GLOBAL_CONFIG_FILENAME = u'global_config.db'

GLOBAL_CONFIG_TEXT_PATH = (u'{path}{sep}global_config'
                           .format(path = GLOBAL_CONFIG_BASE_PATH,
                                   sep = os.sep))

GLOBAL_CONFIG_DATABASES = []


def update_database_filepath(new_base):

    global GLOBAL_CONFIG_DATABASE_FILEPATH

    GLOBAL_CONFIG_DATABASE_FILEPATH = \
        (u'{path}{sep}{filename}'
         .format(path = new_base,
                 sep = os.sep,
                 filename = GLOBAL_CONFIG_FILENAME))

    logging.debug(GLOBAL_CONFIG_DATABASE_FILEPATH)


# Ensure base path is set!
update_database_filepath(GLOBAL_CONFIG_BASE_PATH)

# Ensure This modules database is registered.
add_config_db(db_list=GLOBAL_CONFIG_DATABASES,
              db_path=GLOBAL_CONFIG_DATABASE_FILEPATH)


class GlobalConfigurationDB(BaseConfigurationDB):

    def __init__(self, *args, **kwargs):

        if len(GLOBAL_CONFIG_DATABASES) == 0:
            raise Exception(u'No global databases configured!')

        if len(GLOBAL_CONFIG_DATABASES) > 1:

            update_database_filepath(new_base=CONFIG_TEMP_PATH)

            if not os.path.exists(GLOBAL_CONFIG_DATABASE_FILEPATH):

                logging.info(u'Multiple Global databases, '
                             u'creating temporary global database!')

                gdb = BaseConfigurationDB(GLOBAL_CONFIG_DATABASE_FILEPATH)
                gdb.update_template_data(databases = GLOBAL_CONFIG_DATABASES)

        super(GlobalConfigurationDB, self)\
            .__init__(db_path = GLOBAL_CONFIG_DATABASE_FILEPATH,
                      record_changes = True,
                      *args,
                      **kwargs)


if __name__ == u'__main__':

    db = GlobalConfigurationDB()
    # db.load_config(path=GLOBAL_CONFIG_TEXT_PATH)  # Re-initialise from text files
    db.load_config_updates(path=GLOBAL_CONFIG_TEXT_PATH)  # Update from text files
    db.cleanup()
