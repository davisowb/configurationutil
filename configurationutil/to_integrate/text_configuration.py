__author__ = 'HTH06'

import logging_helper

logging = logging_helper.setup_logging()

from future.builtins import str
import getpass
import os
import codecs
import shutil
from collections import OrderedDict
from tableutil.text_table import (text_table_to_dictionary,
                                  text_table_to_list,
                                  dictionary_to_list)
from tableutil.table import Table
from fdutil.file_utils import make_windows_file_copy_faster
make_windows_file_copy_faster()


BASE_CONFIG_PATH = (u'{int_test_tools}{sep}config{sep}'
                    .format(int_test_tools = os.sep.join(os.path.normpath(__file__).split(os.sep)[:-3]),
                            sep            = os.sep))

GLOBAL_BASE_CONFIG_PATH = (u'{root}global_config{sep}'
                           .format(root = BASE_CONFIG_PATH,
                                   sep  = os.sep))

DEFAULT_BASE_CONFIG_PATH = (u'{root}default_config{sep}'
                            .format(root = BASE_CONFIG_PATH,
                                    sep  = os.sep))

USER_BASE_CONFIG_PATH = (u'{root}user_config{sep}'
                         .format(root = BASE_CONFIG_PATH,
                                 sep  = os.sep))

MODIFIER_BASE_CONFIG_PATH = (u'{root}modifier_config{sep}'
                             .format(root = BASE_CONFIG_PATH,
                                     sep  = os.sep))


def ensure_path_exists(path):
    if not os.path.exists(path):
        os.makedirs(path)


def get_user_path_for_configuration_file(name):
    username = getpass.getuser()
    return u'{root}{username}{sep}{config}.txt'.format(root     = USER_BASE_CONFIG_PATH,
                                                       sep      = os.sep,
                                                       username = username,
                                                       config   = name)


def get_test_path_for_configuration_file(name):
    return u'{root}{name}.txt'.format(root = MODIFIER_BASE_CONFIG_PATH,
                                      name = name)


def get_default_path_for_configuration_file(name):
    return u'{root}{config}.txt'.format(root     = DEFAULT_BASE_CONFIG_PATH,
                                        config   = name,
                                        sep      = os.sep)


def get_global_path_for_configuration_file(name):
    return u'{root}{name}.txt'.format(root = GLOBAL_BASE_CONFIG_PATH,
                                      name = name)


def get_path_for_configuration_file(name):
    paths = (get_user_path_for_configuration_file(name = name),
             get_test_path_for_configuration_file(name = name),
             get_default_path_for_configuration_file(name = name),
             get_global_path_for_configuration_file(name = name))
    for path in paths:
        if os.path.exists(path):
            return path
    raise IOError(u'configuration for {config} not found at {paths}'.format(config = name,
                                                                            paths  = u' or '.join(paths)))


def config_exists(name):
    try:
        get_path_for_configuration_file(name)
        return True
    except IOError:
        return False


def get_dictionary_configuration(name):
    configuration = None
    path = get_path_for_configuration_file(name)

    if path:
        configuration = text_table_to_dictionary(path)
        logging.info(u'configuration for {config} loaded from {path}'.format(config = name,
                                                                             path   = path))
    return configuration


def get_list_configuration(name):
    configuration = None
    path = get_path_for_configuration_file(name)

    if path:
        configuration = text_table_to_list(path)
        logging.info(u'configuration for {config} loaded from {path}'.format(config = name,
                                                                             path   = path))
    return configuration


def get_age_of_configuration_file(name):
    path = get_path_for_configuration_file(name)
    return os.path.getmtime(path)


class DynamicConfig(object):

    u"""
    Provides base class for file based text table configurations that are
    dynamically updated when manually or programmatically modified.

    If a modification is made, the file will be save to the user config.
    """

    def __init__(self,
                 filename,
                 return_dictionary = True,
                 initial_values    = None,
                 auto_save         = True,
                 restrict_to_initial_headings = True):
        """
        :param filename: Name of file containing the configuration file.
                         Don't include a path. The file should be in one
                         of the config directories.

        :param return_dictionary: Boolean. If True, the first field is
                                  used as a key into the list of parameters
        :param initial_values: A data structure containing default values to
                               use to create a new configuration. If this is
                               supplied, an existing matching file in user
                               config will be overwritten.
        :param auto_save: Changes at row level (not cell level) are saved
                          automatically
        :param restrict_to_initial_headings: Boolean. If True, additional
                                             fields are not saved back to
                                             disk.
        """
        self.initial_values = initial_values
        self.auto_save = auto_save
        self.restrict_to_initial_headings = restrict_to_initial_headings
        self.return_dictionary = return_dictionary
        self.use_new_file(filename)

    def use_new_file(self,
                     filename):
        self.filename = filename
        if self.initial_values is not None:
            self.__ensure_user_config_exists()
        try:
            self.title
        except (NameError, AttributeError):
            self.title = [filename]
        self.age_of_file_at_last_load = u"Not loaded"
        self.current_age_of_configuration_file = u"Not Loaded"
        self.__set_defaults()
        self._table = self.table

    def reinitialise_on_file_change(self):
        # Overload if there's a way to detect a file change.
        # Make Subclass specifica checks/changes, then
        # call use_new_file
        pass

    @property
    def __file_has_been_updated(self):
        try:
            self.current_age_of_configuration_file = get_age_of_configuration_file(self.filename)
        except IOError:
            return False

        logging.debug(u'Age of {file} at last load:{age}'.format(file = self.filename,
                                                                 age  = u"Initial Load" if self.age_of_file_at_last_load is None else self.age_of_file_at_last_load))
        logging.debug(u'Age now:{age}'.format(age = self.current_age_of_configuration_file))

        return self.age_of_file_at_last_load != self.current_age_of_configuration_file

    def __set_defaults(self):

        if self.initial_values is not None:
            self._table = self.initial_values.get(u'Data', self.initial_values)
            self.return_dictionary = not isinstance(self._table, list)
            self.keys = self.initial_values.get(u'Headings', self.get_keys())
            self.description = self.initial_values.get(u'Description', [])
            self.update()

            for line in str(self).splitlines():
                logging.debug(line)
            self.save()

    def __ensure_user_config_exists(self):

        global_file = get_global_path_for_configuration_file(self.filename)

        if not os.path.exists(global_file):

            scenario_file = get_test_path_for_configuration_file(self.filename)

            if not os.path.exists(scenario_file):

                user_file = get_user_path_for_configuration_file(self.filename)

                if not os.path.exists(user_file):

                    logging.info(u'User configuration for {config} does not exist, attempting to create!'.format(config = self.filename))

                    default_file = get_default_path_for_configuration_file(self.filename)

                    if os.path.exists(default_file):
                        ensure_path_exists(user_file.replace(u'{n}.txt'.format(n = self.filename), u''))
                        shutil.copy(default_file, user_file)

                    else:
                        raise IOError(u'No default configuration for {config} exists!'.format(config = self.filename))

            else:
                logging.info(u'configuration file {config} is a test case config, skipping check for user config!'.format(config = self.filename))

        else:
            logging.info(u'configuration file {config} is a global config, skipping check for user/test case config!'.format(config = self.filename))

    @property
    def using_key_value_pairs(self):
        return self.return_dictionary and len(self.keys) == 2

    def get_keys(self):
        if self._table:
            if self.return_dictionary:
                self.keys = self._table[self._table.keys()[0]].keys()
            else:
                self.keys = self._table[0].keys()
        else:
            raise ValueError(u'Attempting to initialise from an empty table {filename}'.format(filename = self.filename))

    @property
    def table(self):
        """
        Reads in the config file if it has changed.
        :return: a dictionary or list containing the current config values
        """
        self.reinitialise_on_file_change()

        if not self.initial_values and self.__file_has_been_updated:

            self.age_of_file_at_last_load = get_age_of_configuration_file(self.filename)


            if self.return_dictionary:
                configuration = get_dictionary_configuration(self.filename)
            else:
                configuration = get_list_configuration(self.filename)

            self._table      = configuration[u'Data']
            self.keys        = configuration[u'Headings']
            self.description = configuration[u'Description']
            self.update()

            for line in str(self).splitlines():
                logging.debug(line)
        try:
            return self._table
        except AttributeError:
            raise IOError(u'No config file or default values for {filename}'.format(filename = self.filename))


    def update(self):
        # overload this if you want to do stuff with the changed _table.
        # this will only happen when the file has changed. It will do nothing
        # for default values.
        # If the update is the same for all items, overload update_item instead.
        for item in self:
            self.update_item(item)

    def update_item(self,
                    item):
        # overload this if you want to do stuff when an item is added or modified
        # as a whole.
        # Called for each item when _table is modified.
        # Called for each item added or modified.
        # Note: will not work for parts
        pass

    def _get_table(self):

        def key_value_pair_to_ordered_row(key_value_pair):
            ord_row = OrderedDict()
            ord_row[self.keys[0]] = key_value_pair
            ord_row[self.keys[1]] = self._table[key_value_pair]
            return ord_row

        if self.using_key_value_pairs:
            # Turn into {key:key_name, value_name:value} dictionaties
            table = [key_value_pair_to_ordered_row(row) for row in self._table]

        elif self.return_dictionary:
            table = dictionary_to_list(self._table)
        else:
            table = [row for row in self._table]

        if self.restrict_to_initial_headings:
            restricted_table = []
            for row in table:
                restricted_row = OrderedDict()
                for key, value in iter(row.items()):
                    if key in self.keys:
                        restricted_row[key] = value
                restricted_table.append(restricted_row)
            table = restricted_table

        return Table.init_from_tree(title       = self.title,
                                    tree        = table,
                                    row_numbers = False)

    def __str__(self):
        return self._get_table().text()

    def save(self):
        """
        Saves changes made to the configuration back to the disk.
        Note that if the file was read from general config, the
        modified file is saved to the user config.
        """
        table = self._get_table().text(solid_borders = False, show_title    = False)

        description = u'@ {line}\n'

        with codecs.open(get_user_path_for_configuration_file(self.filename), u'w', encoding = u'utf8') as f:
            f.write(table)
            for line in self.description:
                f.write(description.format(line = line))
        self.age_of_file_at_last_load = get_age_of_configuration_file(self.filename)


    def replace(self,
                table):
        """
        Replaces the entire table contents and saves to disk
        :param table: New contents as dictionary or list
        """
        self._table = table
        if self.auto_save:
            self.save()


    def __iter__(self):
        """
        Allows use of "for x in y" on the table
        Note: Will refresh the table from disk if the file has changed
        """
        for x in self.table:
            yield x

    def __len__(self):
        return len(self.table)

    def __getitem__(self, item):
        """
        Allows use of "table[item]" value
        :param item: list index or dictionary key
        :return: config item at list index or with key
        """
        # TODO: Figure out if by changing this to self.table, __iter__ still works if the table is refreshed.
        #       Currently not refreshing from file on a __getitem__
        return self._table[item]

    def get(self,
            item,
            default = None):
        """
        Allows use of "table[item]" value
        :param item: list index or dictionary key
        :return: config item at list index or with key
        """
        try:
            return self._table[item]
        except (KeyError, IndexError):
            return default


    def __delitem__(self, item):
        """
        Allows use of "del(table[item])" to remove a config item
        :param item: list index or dictionary key
        """
        del(self._table[item])
        if self.auto_save:
            self.save()

    def __setitem__(self,
                    item,
                    value):
        """
        DEBUG INFO: Did you come here from a condition like if DEVICES.endpoints[endpoint][u'API'] == u'TVS' ?
                    Try using if DEVICES.endpoints[endpoint].get(u'API') instead !

        :param item: list index or dictionary key
        :value: New or altered value.

        Allows use of "table[item] = value" to update the table.
        Supply the whole dictionary.
        Will auto save changes to the file.

        e.g. dc = DynamicConfig(...)
             kd1 = OrderedDict()
             kd1['Name'] = 'kd1'
             kd1['IP'] = '1.2.3.4'
             dc['kd1'] = kd1

        Warning: No checks made against existing headers!

        Warning: If you add or modify individual values,
                 these changes won't trigger this method.
                 e.g. dc = DynamicConfig(...)
                      dc['kd1'] = OrderedDict()
                      dc['kd1']['Name']='kd1'
                      dc['kd1']['IP']='1.2.3.4'

        """
        self._table[item] = value
        self.update_item(item)
        if self.auto_save:
            self.save()

    def values(self):
        """
        :return: the config as a new list. Reading/modifying
                 this list will not check for or save changes
                 in the file
        """
        return [self[item] for item in self]

    def append(self, value):
        """
        :param value: Appends value to the configuration list
        """
        self._table += [value]
        if self.auto_save:
            self.save()

    def refresh(self):
        self.table

if __name__ == u"__main__":
    from old_unittests import text_configuration_tests
    text_configuration_tests.run_tests()
