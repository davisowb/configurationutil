
import logging_helper

logging = logging_helper.setup_logging()

import os
from future.builtins import str
from itt_general.configuration import text_configuration
from tableutil.text_table import text_table_to_dictionary
from itt_general.configuration.text_configuration import DynamicConfig

__author__ = 'HTH06'


if False:

    d = DynamicConfig(filename = u'stb_lookup_table',
                      return_dictionary = True)

    0/0
    d = DynamicConfig(filename = u'EDM',
                      return_dictionary = True)

    d[u'LEARNED_ACTIONS_LIMIT'] = 240
    print(str(d))

    d.save()

    0/0
    d = DynamicConfig(filename = u'configuration_files',
                      return_dictionary = True)
    for x in d:
        logging.info(x)
    logging.info(len(d))
    logging.info(d[u'PORT_FORWARD_CONFIG_FILENAME'])
    d = DynamicConfig(filename = u'configuration_files',
                      return_dictionary = False)
    for x in d:
        logging.info(x)
    logging.info(len(d))
    logging.info(d[0])


def test_ensure_path_exists():
    path = u'configuration_test__test_ensure_path_exists'
    os.makedirs(path)
    assert os.path.exists(path)
    os.removedirs(path)
    assert not os.path.exists(path)
    text_configuration.ensure_path_exists(path)
    assert os.path.exists(path)
    os.removedirs(path)
    assert not os.path.exists(path)


def test_create_new_configuration():
    name = u'configuration_test__test_ensure_path_exists'
    path = text_configuration.get_user_path_for_configuration_file(name)
    initial_values = u"""X | Y | Z
                         --+---+---
                         a | b | c
                         d | e | f
                      """
    initial_values = text_table_to_dictionary(initial_values)

    config = text_configuration.DynamicConfig(filename       = name,
                                              initial_values = initial_values)
    config[u'a'][u'W']=u'd'
    config.save()
    del(config)


    blah = text_configuration.DynamicConfig(name)



    os.remove(path)


def run_tests():
    test_ensure_path_exists()
    test_create_new_configuration()


if __name__ == u"__main__":
    run_tests()
